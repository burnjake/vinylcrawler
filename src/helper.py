import os
import tldextract

def create_site_directory(directory_path: str):
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print("Creating directory at: {0}".format(directory_path))

def create_init_files(directory_path: str, base_url: str):
    queue_file = os.path.join(directory_path, "queue.txt")
    crawled_file = os.path.join(directory_path, "crawled.txt")
    error_file = os.path.join(directory_path, "http_errors.txt")
    if not os.path.exists(queue_file):
        write_to_file(queue_file, base_url + "\n")
    if not os.path.exists(crawled_file):
        write_to_file(crawled_file, "")
    if not os.path.exists(error_file):
        write_to_file(error_file, "")

def write_to_file(file_path: str, data: str):
    with open(file_path, "w") as f:
        f.write(data)

def append_to_file(file_path: str, data: str):
    with open(file_path, "a") as f:
        f.write(data + "\n")

def delete_file_contents(file_path: str):
    open(file_path, "w").close()

def file_to_set(file_path: str):
    results = set()
    with open(file_path, 'rt') as f:
        for line in f:
            results.add(line.replace("\n", ""))
    return results

def set_to_file(links: set, file_path: str):
    with open(file_path, "w") as f:
        for l in sorted(links):
            f.write(l + "\n")

def get_domain_name(base_url: str):
    parsed_url = tldextract.extract(base_url)
    return parsed_url.domain + "." + parsed_url.suffix
