import helper
import os
from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup

class Spider:
    queue = set()
    crawled = set()
    error = set()

    def __init__(self, base_url: str, working_directory: str = os.getcwd()):
        self.base_url = base_url
        self.domain_name = helper.get_domain_name(base_url)
        self.project_name = self.domain_name.replace(".", "_")
        self.directory_path = os.path.join(working_directory, self.project_name)
        self.queue_filename = os.path.join(self.directory_path, "queue.txt")
        self.crawled_filename = os.path.join(self.directory_path, "crawled.txt")
        self.error_filename = os.path.join(self.directory_path, "http_errors.txt") 
        self.instantiate()
        self.crawl(base_url)

    def instantiate(self):
        helper.create_site_directory(self.directory_path)
        helper.create_init_files(self.directory_path, self.base_url)
        Spider.queue = helper.file_to_set(self.queue_filename)
        Spider.crawled = helper.file_to_set(self.crawled_filename)
        Spider.error = helper.file_to_set(self.error_filename)

    def crawl(self, url: str):
        if url not in Spider.crawled:
            print("Queue " + str(len(Spider.queue)) + " | Crawled  " + str(len(Spider.crawled)))
            links = self.find_links(url)
            if links:
                self.add_links_to_queue(links)
            Spider.queue.remove(url)
            Spider.crawled.add(url)
            self.update_files()

    def add_links_to_queue(self, urls: str):
        for url in urls:
            if self.is_url_invalid(url):
                continue
            Spider.queue.add(url)
    
    # Added to make the conditions easier to read
    def is_url_invalid(self, url):
        return (
                type(url) != str or 
                url in self.queue or 
                url in self.crawled or
                "http" not in url or
                self.domain_name != helper.get_domain_name(url)
                )

    def update_files(self):
        helper.set_to_file(Spider.queue, self.queue_filename)
        helper.set_to_file(Spider.crawled, self.crawled_filename)
        helper.set_to_file(Spider.error, self.error_filename)

    def find_links(self, url: str):
        try:
            response = urlopen(url)
            if "text/html" in response.getheader("Content-Type"):
                print(response.headers.get_param("charset"), url)
                html_str = response.read().decode("utf-8")
                html_soup = BeautifulSoup(html_str, "lxml")
                return [link.get("href") for link in html_soup.find_all("a")]
            else:
                return []
        except HTTPError as e:
            Spider.error.add(str(e.code) + ", " + url)
            return []


