from spider import Spider
from queue import Queue
from threading import Thread
import helper

def do_crawling(q: Queue, s: Spider):
    while True:
        s.crawl(q.get())
        q.task_done()

def put_links_in_queue(queue_filename: str, q: Queue):
    queue_set = helper.file_to_set(queue_filename)
    if len(queue_set) > 0:
        [q.put(link) for link in queue_set]
        q.join()
        put_links_in_queue(queue_filename, q)

def main():
    # initial_url = "https://www.agrodolce.it/ricette/torta-al-limoncello/"
    initial_url = "http://titan.dcs.bbk.ac.uk/~kikpef01/testpage.html"
    output_dir = "/Users/jakeburn1/Documents/Code/vinylcrawler/output"
    
    s = Spider(initial_url, output_dir)
    q = Queue(maxsize=0)
    num_threads = 4

    for _ in range(num_threads):
        worker = Thread(target=do_crawling, args=(q, s))
        worker.daemon = True
        worker.start()

    put_links_in_queue(s.queue_filename, q)

def test():
    print(helper.get_domain_name("#magazine"))

if __name__ == "__main__":
    main()
    # test()
